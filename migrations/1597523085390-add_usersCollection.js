/**
 * Make any changes you need to make to the database here
 */
const mongoose = require('mongoose')

mongoose.connect(process.env.DATABASE, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false, 
  useUnifiedTopology: true
})

const User  = require('../src/models/user')

async function up () {

  // Já cria alguns valores a serem utilizados para teste 

  // Usuário Comuns 

  await User.create({_id : mongoose.Types.ObjectId("5f3843078e3ec10166b2d6b1"), name: 'Jorge', email: 'jorge@gmail.com', password: 'popo123' })
  await User.create({_id : mongoose.Types.ObjectId("5f3843078e3ec10166b2d6b2"), name: 'Rodrigo', email: 'rodrigo@gmail.com', password: 'popo123' })
  
  // Usuário Administrador 

  await User.create({ _id : mongoose.Types.ObjectId("5f3843078e3ec10166b2d6b3"), name: 'Luna', email: 'luna@gmail.com', password: 'popo123', role: 1 })

}
/**
 * Make any changes that UNDO the up function side effects here (if possible)
 */
async function down () {
  // Write migration here
}

module.exports = { up, down };
