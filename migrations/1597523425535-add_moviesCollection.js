/**
 * Make any changes you need to make to the database here
 */
const mongoose = require('mongoose')

mongoose.connect(process.env.DATABASE, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false, 
  useUnifiedTopology: true
})

const Movie  = require('../src/models/movie')

async function up () {

  // Já cria alguns valores a serem utilizados para teste 

  // Filmes

  await Movie.create({
    _id : mongoose.Types.ObjectId("5fe94c090e1af21733a2e6c1"),
    name: "Avengers: Endgame", 
    description: "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe. ",
    release_year: 2019, 
    director: ["Anthony Russo", "Joe Russo"],
    cast: ["Robert Downey Jr.", "Chris Evans", "Mark Ruffalo", "Chris Hemsworth", "Scarlett Johansson"],
    genre: "Adventure" 
  })

  await Movie.create({
    _id : mongoose.Types.ObjectId("5fe94c090e1af21733a2e6c2"),
    name: "Jurassic Park", 
    description: "A pragmatic paleontologist visiting an almost complete theme park is tasked with protecting a couple of kids after a power failure causes the park's cloned dinosaurs to run loose. ",
    release_year: 1993, 
    director: ["Steven Spielberg"],
    cast: ["Sam Neil", "Laura Dern", "Jeff Goldblum", "Richard Attenborough "],
    genre: "Adventure"
  })

  await Movie.create({
    _id : mongoose.Types.ObjectId("5fe94c090e1af21733a2e6c3"),
    name: "Seven", 
    description: "Two detectives, a rookie and a veteran, hunt a serial killer who uses the seven deadly sins as his motives.",
    release_year: 1995, 
    director: ["David Fincher"],
    cast: ["Morgan Freeman", "Brad Pitt", "Kevin Spacey"],
    genre: "Thriller"
  })

  await Movie.create({
    _id : mongoose.Types.ObjectId("5fe94c090e1af21733a2e6c4"),
    name: "The Silence of the Lambs", 
    description: "A young F.B.I. cadet must receive the help of an incarcerated and manipulative cannibal killer to help catch another serial killer, a madman who skins his victims. ",
    release_year: 1991, 
    director: ["Jonathan Demme"],
    cast: ["Jodie Foster", "Anthony Hopkins", "Lawrence A. Bonney"],
    genre: "Thriller"
  })

  await Movie.create({
    _id : mongoose.Types.ObjectId("5fe94c090e1af21733a2e6c5"),
    name: "Interstellar", 
    description: "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival. ",
    release_year: 2014, 
    director: ["Christopher Nolan"],
    cast: ["Matthew McConaughey", "Anne Hathaway", "Jessica Chastain"],
    genre: "Sci-Fi"
  })

  await Movie.create({
    _id : mongoose.Types.ObjectId("5fe94c090e1af21733a2e6c6"),
    name: "The Matrix", 
    description: "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers. ",
    release_year: 1999, 
    director: ["The Wachowski Brothers"],
    cast: ["Keanu Reeves", "Laurence Fishburne", "Carrie-Anne Moss"],
    genre: "Sci-Fi"
  })

}
/**
 * Make any changes that UNDO the up function side effects here (if possible)
 */
async function down () {
  // Write migration here
}

module.exports = { up, down };
