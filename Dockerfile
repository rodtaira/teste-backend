FROM node:12
WORKDIR /app
COPY package.json /app
RUN npm install
RUN npm install migrate-mongoose -g
COPY . /app
EXPOSE 9000
CMD ["migrate", "up", "add_usersCollection"]
CMD ["migrate", "up", "add_moviesCollection"]
CMD ["npm", "start"]