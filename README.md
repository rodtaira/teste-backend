# Sobre o Projeto

Este é o repositório referente ao teste técnica para a Empresa ioasys de acordo com as especificações dadas no https://bitbucket.org/ioasys/teste-backend/src/master/  


# 🖥 O que desenvolver?

Você deverá criar uma API que o site [IMDb](https://www.imdb.com/) irá consultar para exibir seu conteúdo, sua API deve conter as seguintes features:

- Admin

  - Cadastro
  - Edição
  - Exclusão lógica (Desativação)

- Usuário

  - Cadastro
  - Edição
  - Exclusão lógica (Desativação)

- Filmes

  - Cadastro (Somente um usuário administrador poderá realizar esse cadastro)
  - Voto (A contagem dos votos será feita por usuário de 0-4 que indica quanto o usuário gostou do filme)
  - Listagem (deverá ter filtro por diretor, nome, gênero e/ou atores)
  - Detalhe do filme trazendo todas as informações sobre o filme, inclusive a média dos votos

**Obs.: Apenas os usuários poderão votar nos filmes e a API deverá validar quem é o usuário que está acessando, ou seja, se é admin ou não**

# Configurando o projeto
- Tenha uma versão do node instalada na máquina. A versão utilizada no projeto foi a 12.0.0
- Tenha também uma versão do npm instalada. A versão utilizada no projeto foi a 6.9.0
- Clone o repositório: git clone https://rodtaira@bitbucket.org/ioasys/teste-backend.git 
- Entre na raíz do projeto, abra o terminal e digite npm install para instalar as dependências
- Para executar as migrations, pode ser de duas formas: 
- Instalando de forma global: npm install -g migrate-mongoose
- Feito a instalação, abra o terminal e digite: migrate up add_usersCollection para popular o banco com alguns valores de Usuários e depois digite: migrate up add_moviesCollection para popular o banco com valores de Filmes. Obs: Esteja rodando um banco mongo DB com o comando sudo mongod ou mongod
- A outra maneira seria de forma local digitando no terminal  node_modules/.bin/migrate up add_usersCollection e node_modules/.bin/migrate up add_moviesCollection
- Após isso alguns valores já terão sido inseridos no Banco de Dados

# Rodando o projeto
- Abra o terminal e vá para a raíz do projeto e digite: npm start
- Caso dê tudo certo a API estará sendo executada em http://localhost:8000/api
- Para testar os Endpoints no Postman: https://www.getpostman.com/collections/e21438f70d93fcbbae9e

# Rodando a aplicação com Docker 

- Tenha o docker instalado 
- Entre na raíz do projeto e digite os seguintes comandos: 
- docker build -t teste-ioasys .   
- docker run -p 8000:8000 -t -i  teste-ioasys
