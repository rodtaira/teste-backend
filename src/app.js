const   express             =   require('express')
const   morgan              =   require('morgan')
const   bodyParser          =   require('body-parser')
const   cookieParser        =   require('cookie-parser')
const   expressValidator    =   require('express-validator')
const   cors                =   require('cors')
const   errorHandler        =   require('./helpers/errorHandler');
require('dotenv').config()
require('./db/mongoose')

// Routes Files 

const   authRoutes      =   require('./routes/auth')
const   userRoutes      =   require('./routes/user')
const   movieRoutes     =   require('./routes/movie')

// App

const app = express()

// Middlewares

app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(cookieParser())
app.use(expressValidator())
app.use(cors())

// Routes

app.use('/api',authRoutes)
app.use('/api',userRoutes)
app.use('/api',movieRoutes)


// global error handler
app.use(errorHandler)

// Run Server 

const port = process.env.PORT || 9000

app.listen(port, () => {
    console.log(`Server is running on port ${port}`)
})

module.exports = app

