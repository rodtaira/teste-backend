const mongoose  =   require('mongoose')

mongoose.connect(process.env.DATABASE || "mongodb://localhost:27017/teste-ioasys", {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false, 
    useUnifiedTopology: true
})
.then(()=> {
    console.log("DB Connected")
})