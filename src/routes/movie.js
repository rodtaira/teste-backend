const express   =   require('express')
const router    =   express.Router()

const{ requireSignin, isAuth, isAdmin } = require('../controllers/auth')
const{ userById } = require('../controllers/user')
const{ movieById, createMovie, listMovieSearchBy, listSingleMovie, rateMovie} = require('../controllers/movie')

router.get('/movie/:movieId', listSingleMovie)
router.get('/movies', listMovieSearchBy)
router.post('/movie/:userId', requireSignin, isAuth, isAdmin, createMovie)
router.put('/movie/rating/:movieId/:userId', requireSignin, isAuth, rateMovie)

router.param('userId', userById)
router.param('movieId', movieById)

module.exports = router