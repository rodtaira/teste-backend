const express   =   require('express')
const router    =   express.Router()

const{requireSignin, isAuth, isAdmin} = require('../controllers/auth')
const{userById, readUser, updateUser, delUser} = require('../controllers/user')

router.get('/secret/:userId', requireSignin, isAuth, (req, res) => {
    res.json({
        user:req.profile
    })
})

router.get('/user/:userId', requireSignin, isAuth, readUser)
router.put('/user/:userId', requireSignin, isAuth, updateUser)
router.put('/userAdmin/:userId', requireSignin, isAuth, isAdmin, updateUser)
router.delete('/user/:userId', requireSignin, isAuth, delUser )
router.delete('/userAdmin/:userId', requireSignin, isAuth, isAdmin, delUser )

// ------------------------- Middleware -------------------------

router.param('userId', userById)

module.exports = router