/**
 * Método responsável por calcular a média das notas dos Usuários
 */
module.exports = function calculateAverage(user_reviews, rating){

    let totalSumRate = 0
    let averageRating = rating 

    if(user_reviews.length != 0){

        for(let i = 0; i < user_reviews.length; i++){
            totalSumRate += user_reviews[i].rating
        }

        averageRating = (totalSumRate + rating) / (user_reviews.length + 1)
    }
    
    return averageRating    
}