const mongoose  =   require('mongoose')

const movieSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            maxlength: 200, 
            unique: true
        },
        description: {
            type: String
        },
        genre:{
            type: String,
            maxlength: 32
        },
        director:{
            type: Array
        },
        cast:{
            type: Array
        }, 
        release_year:{
            type: Number
        },
        user_reviews: {
            type: Array,
        },
        reviews_average: {
            type: Number, 
            default: null
        },
    },
    { timestamps: true }
)

module.exports = mongoose.model('Movie', movieSchema)