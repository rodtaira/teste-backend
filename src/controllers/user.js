const User = require('../models/user')
const ifError = require('../utils/ifError')



//Middlewares

/**
 * Representa um método que encontra as informações do usuário via Id. 
 */

exports.userById = (req, res, next, id) => {
    User.findById(id).exec((err, user) => {
        if(err || !user){
            return res.status(400).json({
                error: "User not found"
            })
        }
        req.profile = user
        next()
    })
}

// Controllers

/**
 * Representa um método que retorna as informações do Usuário  
 */

exports.readUser = (req, res) => {
    req.profile.hashed_password =   undefined
    req.profile.salt            =   undefined
    return res.json(req.profile)
}

/**
 * Representa um método que procura e atualizaas informações de uma determinado Usuário.   
 */

exports.updateUser = (req, res) => {

    User.findOneAndUpdate(
        {_id: req.profile._id}, 
        {$set: req.body}, 
        {new: true}, 
        (err, user) => {
            if(err){
                return res.status(400).json({
                    error: 'You are not authorized to perform this action'
                })
            }
            // Por questão de segurança se atribui valores indefinidos em informações sensíveis. 
            user.hashed_password =   undefined
            user.salt            =   undefined
            return res.status(200).json(user)
        }
    )
}

/**
 * Representa um método que procura um determinado usuário e deleta as informações do mesmo  
 */

exports.delUser = (req, res) => {
    
    const user = req.profile

    User.find({ user }).exec((err) => {
        if (user.length >= 1) {
            return res.status(400).json({
                message: 'Sorry. You cant delete User' 
            })
        } else {
            user.remove((err, user) => {
                if (err) {
                    return ifError(err, res)
                }
                return res.status(200).json({
                    message: 'User deleted',
                    user_info: user
                })
            })
        }
    })
}
