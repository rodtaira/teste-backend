const Movie = require('../models/movie')
const calculateAverage = require('../utils/calculateAverage')

// Middleware

/** 
 * Representa um método que encontra as informações de um filme via Id. 
 */

exports.movieById = (req, res, next, id) => {
    Movie.findById(id).exec((err, movie) => {
        if(err || !movie){
            return res.status(400).json({
                error: "Movie not found"
            })
        }
        req.movie = movie
        next()
    })
}

//Controllers 

/** 
 * Representa um método que cadastrar as informações de um novo filme.  
 */


exports.createMovie = (req, res) => {

    const movie = new Movie(req.body)
    
    movie.save((err, movie) => {
        if (err) {
            return res.status(400).json({error: err})
        }
       return res.status(201).json({movie})
   })
}

/** 
 * Representa um método que lista as informações de um único filme.  
 */

exports.listSingleMovie = (req, res) => {

    const movieId   =   req.movie._id

    Movie.find( {_id: movieId} , (err, movie) => {
        if (err) {
            return res.status(400).json({error: err})
        }
       return res.status(201).json({movie})
    });
}

/** 
 * Representa um método que lista as informações de um filme de acordo com algumas informações que como listar po direto, gênero, ano de lançamento e etc.  
 */

exports.listMovieSearchBy = (req, res) => {

    const searchBy    =   req.query.searchBy
    const search      =   req.query.search

    const objectSearch = {[searchBy]: search}
    console.log(objectSearch)

    Movie.find( objectSearch, (err, movies) => {
        if (err) {
            return res.status(400).json({error: err})
        }
       return res.status(201).json({movies})
    });
}

/**
 * Representa um método que irá pegar a nota que um usuário deu para um determinado filme e guardar essa informação e fazer a média das notas de todas as avaliações disponíveis. 
 */

exports.rateMovie = (req, res) => {

    const   userId      =   req.profile._id
    const   movieId     =   req.movie._id
    let     userReviews =   req.movie.user_reviews 
    let     rating      =   req.body.rating

    if(rating < 0 || rating > 4){
        return res.status(412).json({error: "Rate higher or lower from the requested range "})
    }else{
        let averageRating = calculateAverage(userReviews, rating)

        const objRating = {userId: userId, rating: rating  }
        const objRatingAverage = {reviews_average: averageRating}
       
        Movie.findOneAndUpdate(
            { _id: movieId}, 
            { $push: { user_reviews: objRating}, $set: objRatingAverage},
            {new: true},
            (err, movie) => {
                if (err) {
                    return res.status(400).json({error: err})
                }
            return res.status(201).json({movie})
            }
        )
    }
}
