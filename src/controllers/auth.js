    
const jwt               =   require('jsonwebtoken')
const expressJwt        =   require('express-jwt')
const {errorHandler}    =   require('../helpers/dbErrorHandler')
const User              =   require('../models/user')

/**
 * Representa um método para cadastrar um novo Usuário 
*/

exports.signUp = (req, res) => {

    const user = new User(req.body)
    
    user.save((err, user) => {
        if(err){
            return res.status(400).json({
                err: errorHandler(err)
           }) 
       }
       return res.status(201).json({user})
   })
}

/**
 * Representa um método para cadastrar um novo usuário administrador
*/

exports.signupAdmin = (req, res) => {

    const user = new User(req.body)
    user.role = 1
    
    user.save((err, user) => {
        if(err){
            return res.status(400).json({
                err: errorHandler(err)
           }) 
       }
       return res.status(201).json({user})
   })
}

/**
 * Representa um método Sign In utilizando autenticação JWT responsável por Logar um usuário e gerar um token 
*/

exports.signIn = (req, res) => {

    // Procura o Usuário usando o Email 

    const { email, password } = req.body;
    User.findOne({ email }, (err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: 'User with that email does not exist. Please signup'
            })
        }
        // Caso o Usuário seja encontrando, assegurar que a senha e email estão corretos. 
    
        if (!user.authenticate(password)) {
            return res.status(401).json({
                error: 'Email or password do not match'
            })
        }

        // Cria token de autenticação 
        const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET)
        // Guarda token em um cookie 'token' com tempo para expirar
        res.cookie('token', token, { expire: new Date() + 9999 })
        // retornar a resposta com o Usuário e o token gerado 
        const { _id, name, email, role } = user
        return res.status(200).json({ token, user: { _id, email, name, role } })
    })
}

/**
 * Representa um método responsável por Deslogar o usuário limpando os cookies no qual estava gravado o token de autenticação gerado pelo jwt.  
*/

exports.signOut = (req, res) => {
    res.clearCookie('token');
    res.json({ message: 'Signout success' });
}

// Middlewares 

/**
 * Representa um método que requisita que o usuário esteja logado 
 */

exports.requireSignin = expressJwt({
    secret: process.env.JWT_SECRET, 
    userProperty: "auth"
})

/**
 * Representa um método que confirma que o usuário está logado. 
 */

exports.isAuth = (req, res, next) => {
    
    let user = req.profile && req.auth && req.profile._id == req.auth._id

    if(!user){
        return res.status(403).json({
            error: "User not Sign In !"
        })
    }

    next()
}

/**
 * Representa um método que confirma que o usuário que está logado é também usuário administrador. 
 */

exports.isAdmin = (req, res, next) => {
    
    if(req.profile.role === 0){
        return res.status(403).json({error : "Admin resources. Access denied !"})
    }
    next()
} 